// Server_Listener.java
import java.io.*;
import java.net.*;

public class Server_Listener {
	
	//indico la porta attraverso cui voglio comunicare
	public static final int PORT = 1050; // porta al di fuori del range 1-1024 !
	
	public static void main(String[] args) throws IOException {
		//creo il socket del server sulla porta impostata
		ServerSocket serverSocket = new ServerSocket(PORT);
		
		System.out.println("Server: Avviato. Socket info: " + serverSocket);
		
		//inizializzo il socket per il client
		Socket clientSocket = null;
		
		BufferedReader in = null;
		
		try {
			System.out.println("Server: Sono in attesa di un client");
			
			//attendo che un client richieda una connessione e mi collego
			clientSocket = serverSocket.accept();
			
			System.out.println("Connessione accettata a: "+ clientSocket);
			
			// creazione stream (canale) di input dal clientSocket
			InputStreamReader isr = new InputStreamReader(clientSocket.getInputStream());
			in = new BufferedReader(isr);
			
			System.out.println("Canale di input creato");
			System.out.println("Verranno ora trascritti i messaggi del client\n");
			
			//ciclo di ricezione dal client
			while (true) {
				//leggo i messaggi inviati dal client
				String str = in.readLine();
				
				if (str.equals("END"))	//se la stringa del client � "END"
					break;				//interrompo il ciclo di comunicazione
				
				//stampo a schermo il messaggio ricevuto e indico la risposta
				System.out.println("Client: " + str);
			}
		}
		catch (IOException e) {
			System.err.println("Accettattazione fallita");
			System.exit(1);
		}
		
		// se il ciclo di comunicazione � stato interrotto,
		// eseguo la chiusura di stream e socket
		System.out.println("Server: Arresto in corso...");
		in.close();
		clientSocket.close();
		serverSocket.close();
		System.out.println("Server: Arresto completato");
	}
} 

// Server_Listener